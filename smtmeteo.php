<?php
/**
 * Plugin Name: SMT Meteo
 * Plugin URI: http://bitbucket.org/swissmediatools/smt-meteo
 * Description: Intégration et affichage des données météos d'OpenWeatherMap
 * Version: 1.0
 * Author: Swiss Media Tools
 * Author URI: http://swissmediatools.ch
 * License: GPL
 * Bitbucket Plugin URI: https://swissmediatools@bitbucket.org/swissmediatools/smt-meteo.git
 * Bitbucket Branch: master
 */
 
 	global $smtdebug;
 	$smtdebug="Debug SMT Meteo - ".date("d.m.Y G:i:s",current_time('timestamp'))."<br>";
	
	global $theuser;
	$theuser = array();
	
	global $weekdays,$weekdaysab,$cesjours,$lesmois,$lesmoisab;
	$weekdays=array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
	$weekdaysab=array("Dim","Lun","Mar","Mer","Jeu","Ven","Sam");
	$cesjours=array("Aujourd'hui","Demain","Après-demain","Jour","Jours");
	$lesmois=array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
	$lesmoisab=array("JAN.","FEV.","MARS","AVR.","MAI","JUIN","JUIL.","AOÛT","SEP.","OCT.","NOV.","DEC.");
	
	//	global $current_user;
    //	$current_user = wp_get_current_user();
	
	/*
	
	$smtdebug.="========== USER INFO ==========<br>";
	
	if ( 0 == $current_user->ID ) {
		$theuser['loggedin']=1;
		$theuser['username']=$current_user->user_login;
		$theuser['userid']=$current_user->ID;
		$theuser['isadmin']=intval(current_user_can('administrator'));
		$smtdebug.="Utilisateur loggué : ".$theuser['username']." (".$theuser['userid']."), Admin = ".$theuser['isadmin']."<br>";
	} else {
		$theuser['loggedin']=0;
		$smtdebug.="L'utilisateur n'est pas loggué ...<br>";
	}
	
	*/
 
 	//	Création de la page de settings
	
	if (!isset($_SESSION['smtmeteodebug']))	{	$_SESSION['smtmeteodebug']=0;	}
	
	$_SESSION['smtmeteodebug'] = 0 ;
	
	//	Ajout du style
	
	wp_enqueue_style('smtmeteo-style', plugin_dir_url(__FILE__)."smtmeteo.css");
	
	//	Intégration de la page de Gestion des paramêtres du Plugin
	
	function smtmeteo_plugin_menu()	
		{	
			add_options_page('SMTmeteo', 'SMT-Meteo', 'manage_options', 'smtmeteo-plugin-menu', 'smtmeteo_plugin_options');
			add_action( 'admin_init', 'register_smtmeteo_settings' );
		}
	add_action('admin_menu','smtmeteo_plugin_menu');
	function smtmeteo_plugin_options()	
	{	
		include('smtmeteo-admin.php');	
	}
	function register_smtmeteo_settings() 
	{
		register_setting( 'smtmeteo-settings-group', 'owm_api_key' ); 
		register_setting( 'smtmeteo-settings-group', 'owm_cachemin' );
		register_setting( 'smtmeteo-settings-group', 'owm_gpslat' );
		register_setting( 'smtmeteo-settings-group', 'owm_gpslon' ); 
		register_setting( 'smtmeteo-settings-group', 'owm_units' ); 
		register_setting( 'smtmeteo-settings-group', 'smt_update' ); 
	}
	
	
		
		//	Fonctions Meteo
		
		global $owmapi;
		$owmapi = array();
		
		$owmapi['api']="http://api.openweathermap.org/data/2.5/";
		$owmapi['key']=get_option('owm_api_key');
		$owmapi['units']=get_option('owm_units');
		$owmapi['cache']=intval(get_option('owm_cachemin'));
		$owmapi['gpslat']=floatval(get_option('owm_gpslat'));
		$owmapi['gpslon']=floatval(get_option('owm_gpslon'));
		
		function addFooter($content) {
			$content .= '<div id=\"\smtmeteo_footer">Plugin SMT-Meteo par <A HREF=\"http://swissmediatools.ch/plugin-smtmeteo\" target=\"_blank\">SwissMediaTools</a></div>';
		return $content;
		} 
		
		function convert_time_from_gmt ($thetime)
			{
				$wptime_gmt = current_time( 'timestamp' , 1 );
				$wptime_local = current_time( 'timestamp' , 0 );
				$wptime_diff = $wptime_gmt - $wptime_local;
				$thetime=$thetime;
				$thetimeout=$thetime - $wptime_diff;
				return $thetimeout;
			}
		
		function smtmeteo_get_data($mode,$lang="fr")
			{
				global $owmapi;
				global $smtdebug;
				$smtdebug.="========== CACHE FILE ==========<br>";
				$usecache=0;
				$thecachefile=plugin_dir_path(__FILE__)."cache/".$mode.".xml";
				$smtdebug.="path : ".$thecachefile."<br>";
				if ($mode=="actuel")
			 		{
						$owmthefullurl=$owmapi['api']."weather?lat=".$owmapi['gpslat']."&lon=".$owmapi['gpslon']."&lang=".$lang."&mode=xml&units=".$owmapi['units']."&APPID=".$owmapi['key'];
					}
				if ($mode=="5j")
			 		{
						$owmthefullurl=$owmapi['api']."forecast?lat=".$owmapi['gpslat']."&lon=".$owmapi['gpslon']."&lang=".$lang."&mode=xml&units=".$owmapi['units']."&APPID=".$owmapi['key'];
					}
				if ($mode=="15j")
			 		{
						$owmthefullurl=$owmapi['api']."forecast/daily?lat=".$owmapi['gpslat']."&lon=".$owmapi['gpslon']."&lang=".$lang."&mode=xml&units=".$owmapi['units']."&cnt=15&APPID=".$owmapi['key'];
					}
				
				$smtdebug.="Test du fichier : ";
				if (file_exists($thecachefile))
					{
						$smtdebug.=" Existe !";
						$thecachetime=filemtime($thecachefile);
						$thecachedelay=time()-($owmapi['cache']*60);
						if ($thecachetime>$thecachedelay)
							{
								$usecache=1;
							}
						$smtdebug.=" Délai Cache (".$owmapi['cache']." min/".($owmapi['cache']*60)." sec) , ".date("d.m.Y G:i:s",$thecachetime)." : ".($thecachetime-$thecachedelay);
					} else {
						$smtdebug.=" N'existe pas !";
					}
				
				$smtdebug.="<br>";
				
				if ($usecache==1)
					{
						//	Utilisation des données en Cache
						$dataxml=file_get_contents($thecachefile);	
						$smtdebug.="Utilisation du fichier en cache<br>";
					} else {
						//	Utilisation des données distantes
						$dataxml=file_get_contents($owmthefullurl);
						$smtdebug.="Utilisation des données distantes<br>url : ".$owmthefullurl."<br>";
						file_put_contents($thecachefile,$dataxml);
					}
				
				$meteodata=simplexml_load_string($dataxml);
				return $meteodata;
			}
		
		//	Shortcodes
		
		function smtmeteo_show_shortcode ($atts) {
			//	Récupération des valeurs du shortcode
			extract(shortcode_atts(array(
				  'mode' => 'actuel',
				  'width' => 500
			   ), $atts));
			 //	print("Shortcode SMTMETEO demandé, mode = ".$mode.", width  ".$width."<fini>");
			 global $smtdebug;
			 global $owmapi;
			 global $weekdays,$weekdaysab,$cesjours,$lesmois,$lesmoisab;
			 
			 $smtdebug.="Shortcode SMTMETEO demandé, mode = ".$mode.", width  ".$width."<br>Demande des données :<br>";
			 
			 //	Collecte des données
			 $meteodata = smtmeteo_get_data($mode);
			 
			 $sortiemeteo="<div id=\"smtmeteo_maindiv\" style=\"width:".$width."px;float:left;\">";
			 
			 if ($mode=="actuel")
			 	{
					$meteodata_lastupdate=strtotime($meteodata->lastupdate['value']);
					$sortiemeteo.="<h2>Mesures actuelles (".date("G:i",convert_time_from_gmt($meteodata_lastupdate)).")</h2>";
					//	Picto actuel
					$sortiemeteo.="<div class=\"smtmeteo_actuel_pictodiv\"><img src=\"".plugin_dir_url(__FILE__)."images/owmicons/".strval($meteodata->weather['icon']).".png\"><p>".strval($meteodata->weather['value'])."</p></div>";
					//	Température
					$sortiemeteo.="<div class=\"smtmeteo_actuel_tempdiv\">";
					$sortiemeteo.="<p class=\"smtmeteo_divtitle\">Température</p>";
					$sortiemeteo.="<div class=\"smtmeteo_actuel_tempmain\"><B>".round(floatval($meteodata->temperature['value']),1)."</B>&nbsp;°</div>";
					$sortiemeteo.="<p align=center><font class=smtmeteo_actu_tempmin>min: <b>".round(floatval($meteodata->temperature['min']),1)." °</b></font><br><font class=smtmeteo_actu_tempmax>max: <b>".round(floatval($meteodata->temperature['max']),1)." °</b></font></p>"; 
					$sortiemeteo.="</div>";
					$sortiemeteo.="<div class=\"smtmeteo_actuel_mesurdiv\">";
					$sortiemeteo.="<p class=\"smtmeteo_divtitle\">Autres Mesures</p>";
					$sortiemeteo.="<p class=\"smtmeteo_mesur\">Pression : <b>".round(floatval($meteodata->pressure['value']),0)."</b> ".$meteodata->pressure['unit']."<br>Humidité : <b>".round(floatval($meteodata->humidity['value']),0)."</b> %<br>Nuages : <b>".round(floatval($meteodata->clouds['value']),0)."</b> %<br>Précipitation : <b>".round(floatval($meteodata->precipitation['value']),1)."</b></p>";
					$sortiemeteo.="</div>";
					$sortiemeteo.="<div class=\"smtmeteo_actuel_ventdiv\">";
					$sortiemeteo.="<p class=\"smtmeteo_divtitle\">Vent</p>";
					$sortiemeteo.="<p class=\"smtmeteo_mesur\">Vitesse : <b>".floatval($meteodata->wind->speed['value'])."</b> m/s | ".floor(floatval($meteodata->wind->speed['value'])*3.6)." km/h<br><i>".strval($meteodata->wind->speed['name'])."</i><br>Direction : <b>".floatval($meteodata->wind->direction['value'])."</b> ° <b>".strval($meteodata->wind->direction['code'])."</b> (".strval($meteodata->wind->direction['name']).")</p>";
					$sortiemeteo.="</div>";
					$sortiemeteo.="<div class=\"clear\"></div>";
					$sortiemeteo.="<p class=\"smtmeteo_actuel_timestamp\">Ces mesures ont été effectuées le ".date("d.m.Y",convert_time_from_gmt($meteodata_lastupdate))." à ".date("G:i",convert_time_from_gmt($meteodata_lastupdate)).".</p>";
					
					$smtdebug.="Last update Meteo Data : ".$meteodata_lastupdate." , ".date("d.m.Y G:i:s",$meteodata_lastupdate)."<br>";
					
				}
			
			if ($mode=="5j")
			 	{
					$sortiemeteo.="<h2>Prévisions à 5 Jours</h2>";
					
					$prevs = array() ;
					$actualday = 0 ;
					$actualdayid = 0 ;
					
					$sortiemeteo.="<table class=\"smtmeteo_prev5_maintable\" width=\"100%\">";
					
					
					foreach ($meteodata->forecast->time as $timeitem)
						{
							$timeitem_fr=strtotime($timeitem['from']);
							$timeitem_to=strtotime($timeitem['to']);
							
							$timeitem_day=date("Ymd",$timeitem_fr);
							
							if ($actualday==0)	
								{	
									$actualday=$timeitem_day;
									$cptprevs=0;
									$prevs[$actualdayid]['date']=intval($timeitem_fr);
									$prevs[$actualdayid]['affdate']=date("d.m",$timeitem_fr);
									$prevs[$actualdayid]['affweekday']=$weekdays[date("w",$timeitem_fr)];
								} else {	
									if ($actualday<>$timeitem_day)
										{
											$actualday=$timeitem_day;
											$cptprevs=0;
											$actualdayid++;
											$prevs[$actualdayid]['date']=intval($timeitem_fr);
											$prevs[$actualdayid]['affdate']=date("d.m",$timeitem_fr);
											$prevs[$actualdayid]['affweekday']=$weekdays[date("w",$timeitem_fr)];
										}
								}
							
							$prevs[$actualdayid]['times'][$cptprevs]['fr_time']=date("G:i",$timeitem_fr);
							$prevs[$actualdayid]['times'][$cptprevs]['to_time']=date("G:i",$timeitem_to);
							$prevs[$actualdayid]['times'][$cptprevs]['picto']=strval($timeitem->symbol['var']);
							$prevs[$actualdayid]['times'][$cptprevs]['picto_name']=strval($timeitem->symbol['name']);
							$prevs[$actualdayid]['times'][$cptprevs]['picto_number']=intval($timeitem->symbol['number']);
							$prevs[$actualdayid]['times'][$cptprevs]['precip']=floatval($timeitem->precipitation['value']);
							$prevs[$actualdayid]['times'][$cptprevs]['precip_unit']=strval($timeitem->precipitation['unit']);
							$prevs[$actualdayid]['times'][$cptprevs]['precip_type']=strval($timeitem->precipitation['type']);
							$prevs[$actualdayid]['times'][$cptprevs]['vent']=round(floatval($timeitem->windSpeed['mps'])*1.609344,1);
							$prevs[$actualdayid]['times'][$cptprevs]['vent_name']=strval($timeitem->windSpeed['name']);
							$prevs[$actualdayid]['times'][$cptprevs]['ventdir']=floatval($timeitem->windDirection['deg']);
							$prevs[$actualdayid]['times'][$cptprevs]['ventdir_name']=strval($timeitem->windDirection['name']);
							$prevs[$actualdayid]['times'][$cptprevs]['ventdir_code']=strval($timeitem->windDirection['code']);
							$prevs[$actualdayid]['times'][$cptprevs]['temp']=floatval($timeitem->temperature['value']);
							$prevs[$actualdayid]['times'][$cptprevs]['temp_min']=floatval($timeitem->temperature['min']);
							$prevs[$actualdayid]['times'][$cptprevs]['temp_max']=floatval($timeitem->temperature['max']);
							$prevs[$actualdayid]['times'][$cptprevs]['temp_unit']=strval($timeitem->temperature['unit']);
							$prevs[$actualdayid]['times'][$cptprevs]['press']=floatval($timeitem->pressure['value']);
							$prevs[$actualdayid]['times'][$cptprevs]['press_unit']=strval($timeitem->pressure['unit']);
							
							$prevs[$actualdayid]['times'][$cptprevs]['nuages']=intval($timeitem->clouds['all']);
							$prevs[$actualdayid]['times'][$cptprevs]['precip']=intval($timeitem->precipitation['value']);

							$cptprevs=$cptprevs+1;
						}
						
					$smtdebug.= "Prev 5J / 3h : ".count($prevs)." items<br>";
					
					$cptdays=0;
					
					foreach ($prevs as $theday)
						{
							$theday_name=$theday['affdate'];
							$theday_weekday=$theday['affweekday'];
							$theday_stamp=intval($theday['date']);
							$theday_mois=date("m",$theday_stamp);
							$theday_mois_affab=$lesmoisab[($theday_mois-1)];
							
							$sortiemeteo.="<tr><td class=\"smtmeteo_prev5_date\">";
							
							//	$sortiemeteo.="<div class=\"smtmeteo_prevline_maindiv\">";
							//	$sortiemeteo.="<div class=\"smtmeteo_prevline_datediv\" style=\"width:12%;\">";
							$sortiemeteo.="<p><span class=\"smtmeteo_prevline_weekday\">".strtoupper($theday_weekday)."</span><br><span class=\"smtmeteo_prevline_date\">".date("d",$theday_stamp)."</span><br><span class=\"smtmeteo_prevline_monab\">".$theday_mois_affab."</span>";
							if ($cptdays<3)	{	$sortiemeteo.="<br><span class=\"smtmeteo_prevline_cesjours\">".$cesjours[$cptdays];	} else {	$sortiemeteo.="<br><span class=\"smtmeteo_prevline_cesjours\">".$cptdays." ".$cesjours[4];	}
							$sortiemeteo."</span></p>";
							//	$sortiemeteo.="</div>";
							
							$sortiemeteo.="</td><td class=\"smtmeteo_prev5_tdprev\"><table class=\"smtmeteo_prev5_prevline\">";
							
							$tableheaderstyle="font-size:11px;border:none; border-bottom:1px solid; padding:1px;";
							
							$sortiemeteo.="<tr><td style=\"width:8%;".$tableheaderstyle."\" align=\"center\"><img src=\"".plugin_dir_url(__FILE__)."images/icon_tdhead_hour.png\" class=\"smtmeteo_prev5_iconheader\"></td><td style=\"width:27%;".$tableheaderstyle."\" align=\"center\"><img src=\"".plugin_dir_url(__FILE__)."images/icon_tdhead_meteo.png\" class=\"smtmeteo_prev5_iconheader\"></td><td style=\"width:23%;".$tableheaderstyle."\" align=\"center\"><img src=\"".plugin_dir_url(__FILE__)."images/icon_tdhead_temperature.png\" class=\"smtmeteo_prev5_iconheader\"></td><td style=\"width:23%;".$tableheaderstyle."\"  align=\"center\"><img src=\"".plugin_dir_url(__FILE__)."images/icon_tdhead_wind.png\" class=\"smtmeteo_prev5_iconheader\"></td><td style=\"width:8%;".$tableheaderstyle."\"  align=\"center\"><img src=\"".plugin_dir_url(__FILE__)."images/icon_tdhead_clouds.png\" class=\"smtmeteo_prev5_iconheader\"></td><td style=\"width:8%;".$tableheaderstyle."\"  align=\"center\"><img src=\"".plugin_dir_url(__FILE__)."images/icon_tdhead_precipitation.png\" class=\"smtmeteo_prev5_iconheader\"></td></tr>";
							
							$cptdays++;
							
							$prevlineback="a";
							
								foreach ($theday['times'] as $thetime)
									{
										$thetimestyle="";
										//	$sortiemeteo.="<div class=\"smtmeteo_prevline_timediv\" style=\"width:9%;\" align=\"center\">";
										//	$sortiemeteo.="<p class=\"smtmeteo_prevline_timefr\">".$thetime['fr_time']."</p>";
										//	$sortiemeteo.="<img src=\"".plugin_dir_url(__FILE__)."images/owmicons/".strval($thetime['picto']).".png\" width=\"50\" alt=\"".$thetime['picto_name']."\" title=\"".$thetime['picto_name']."\">";
										//	$sortiemeteo.="<p class=\"smtmeteo_prevline_temp\"><b>".round($thetime['temp'],1)."</b> °</p>";
										//	$sortiemeteo.="</div>";
										$sortiemeteo.="<tr style=\"font-size:11px;\"><td class=\"smtmeteo_prev5_backline_".$prevlineback."\" style=\"text-align:right;\">".$thetime['fr_time']."</td><td class=\"smtmeteo_prev5_backline_".$prevlineback."\">".$thetime['picto_name']."</td><td class=\"smtmeteo_prev5_backline_".$prevlineback."\"><b>".round($thetime['temp'],1)."</b> ° | <span class=smtmeteo_actu_tempmin>".round($thetime['temp_min'],1)."</span> > <span class=smtmeteo_actu_tempmax>".round($thetime['temp_max'],1)."</span></td><td class=\"smtmeteo_prev5_backline_".$prevlineback."\"><b>".$thetime['vent']."</b> m/s , ".floor($thetime['vent']*3.6)." km/h</td><td class=\"smtmeteo_prev5_backline_".$prevlineback."\"><b>".$thetime['nuages']."</b> %</td><td><b>".$thetime['precip']."</b> mm</td></tr>";
										
										if ($prevlineback=="a")	{	$prevlineback="b";	} else {	$prevlineback="a";	}
									}
									
							//	$sortiemeteo.="</div>";
							
							$sortiemeteo.="</table></td></tr>";
						}
						
						$sortiemeteo.="</table>";
						
					/*
					print("<pre>");
					print_r($prevs);
					print("</pre>");
					*/
					
				}
			
			if ($mode=="15j")
			 	{
					$sortiemeteo.="<h2>Prévisions à 15 Jours</h2>";
					$cptdays=0;
					foreach ($meteodata->forecast->time as $timeitem)
						{
							$timeitem_day=strtotime($timeitem['day']);
							$timeitem_affjour=$weekdays[date("w",$timeitem_day)];
							$timeitem_affmois=$lesmois[date("n",$timeitem_day)-1];
							$timeitem_picto=$timeitem->symbol['var'];
							
							$sortiemeteo.="<div class=\"smtmeteo_prevline_maindiv\">";
							$sortiemeteo.="<div class=\"smtmeteo_prevline_qdatediv\" style=\"width:15%;height:110px;\">";
							$sortiemeteo.="<p><span class=\"smtmeteo_prevline_qweekday\">".$timeitem_affjour."</span><br><span class=\"smtmeteo_prevline_date\">".date("d",$timeitem_day)."</span><br><span class=\"smtmeteo_prevline_monab\">".$timeitem_affmois."</span>";
							if ($cptdays<3)	{	$sortiemeteo.="<br><span class=\"smtmeteo_prevline_cesjours\">".$cesjours[$cptdays];	} else {	$sortiemeteo.="<br><span class=\"smtmeteo_prevline_cesjours\">+ ".$cptdays." ".$cesjours[4];	}
							$sortiemeteo."</span></p>";
							$sortiemeteo.="</div>";
							$sortiemeteo.="<div class=\"smtmeteo_prevline_daydiv\" style=\"width:15%;height:110px;\" align=\"center\"><img src=\"".plugin_dir_url(__FILE__)."images/owmicons/".$timeitem_picto.".png\" vspace=\"15\"></div>";
							$sortiemeteo.="</div>";
							$cptdays++;
						}
				}
			
			 
			 
			 $sortiemeteo.="<div id=\"clear\"></div>";
			 $sortiemeteo.="</div>";
			 //	$sortiemeteo.="<div id=\"smtlink_to_plugin\"><A HREF=http://swissmediatools.ch/plugin-smtmeteo target=_blank>Plugin SMTMETEO pour Wordpress par SwissMediatools</A></div>";
			 
			 if ($_SESSION['smtmeteodebug']==1)
			 	{
					$sortiemeteo.="<div><h3>DEBUG</h3><PRE>".$smtdebug."</PRE></div>";	
				}
			 
			 return $sortiemeteo;
			 
		}
		
		add_shortcode('smtmeteo', 'smtmeteo_show_shortcode');
		
		//	Widget
		
		// Creating the widget 
		class smtmeteo_widget extends WP_Widget {
		
		function __construct() {
		parent::__construct(
		// Base ID of your widget
		'smtmeteo_widget', 
		
		// Widget name will appear in UI
		__('SMT Meteo', 'smtmeteo_widget_titre'), 
		
		// Widget description
		array( 'description' => __( 'Ce widget présente la météo actuelle et une brève prévision à 5 jours', 'smtmeteo_widget_titre' ), ) 
		);
		}
		
		// Creating widget front-end
		// This is where the action happens
		public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		
		// This is where you run the code and display the output
		//	echo __( 'Hello, World!', 'smtmeteo_widget_titre' );

			$themode=$instance['mode'];

			//	Capture des données
			$meteodata = smtmeteo_get_data($themode);
		
			$sortiemeteo.="<div class=\"smtmeteo_widget_maindiv\">";
			
			
			if ($themode=="actuel")
				{
					$sortiemeteo.="<img src=\"".plugin_dir_url(__FILE__)."images/owmicons/".strval($meteodata->weather['icon']).".png\" width=\"60\" align=\"left\" style=\"margin-right:5px;\"><p>".strval($meteodata->weather['value'])."<br><span class=\"smtmeteo_widget_tempmain\">".round(floatval($meteodata->temperature['value']),1)."°</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"smtmeteo_widget_tempminmax\">(<span class=\"smtmeteo_widget_tempmin\">".round(floatval($meteodata->temperature['min']),1)."°</span>|<span class=\"smtmeteo_widget_tempmax\">".round(floatval($meteodata->temperature['max']),1)."°</span>)</span><br><span class=\"smtmeteo_widget_mesures\"><b>".round(floatval($meteodata->pressure['value']),0)."</b> ".$meteodata->pressure['unit']." | H: <b>".round(floatval($meteodata->humidity['value']),0)."</b> %<br>N: <b>".round(floatval($meteodata->clouds['value']),0)."</b> % | P: <b>".round(floatval($meteodata->precipitation['value']),1)."</b></span></p>";	
				}
			$sortiemeteo.="</div>";
			
			if ($instance['pagemeteo']<>'')
				{
					$sortiemeteo.="<div class=\"smtmeteo_widget_lienpagediv\"><A HREF=\"".$instance['pagemeteo']."\">Plus de Météo</A></div>";	
				}
			
			echo $sortiemeteo;
			
		
		echo $args['after_widget'];
		}
				
		// Widget Backend 
		public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {	$title = $instance[ 'title' ];	} else { $title = __( 'Nouveau titre', 'smtmeteo_widget_titre' ); }
		if ( isset( $instance[ 'mode' ] ) ) {	$mode = $instance[ 'mode' ];	} else { $mode = __( 'actuel', 'smtmeteo_widget_mode' ); }
		if ( isset( $instance[ 'pagemeteo' ] ) ) {	$pagemeteo = $instance[ 'pagemeteo' ];	} else { $pagemeteo = __( '', 'smtmeteo_widget_pagemeteo' ); }
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Titre:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        <p>
		<label for="<?php echo $this->get_field_id( 'mode' ); ?>"><?php _e( 'Mode:' ); ?></label> 
		<select class="widefat" id="<?php echo $this->get_field_id( 'mode' ); ?>" name="<?php echo $this->get_field_name( 'mode' ); ?>" size="1"><?
        	$themode=esc_attr( $mode );
			
			if ($themode=="actuel")
				{
					?><option value="actuel" selected>Temps Actuel</option><?
				} else {
					?><option value="actuel">Temps Actuel</option><?
				}
			
			if ($themode=="5j")
				{
					?><option value="5j" selected>Prévision à 5 Jours</option><?
				} else {
					?><option value="5j">Prévision à 5 jours</option><?
				}
			
			if ($themode=="15j")
				{
					?><option value="15j" selected>Prévision à 15 Jours</option><?
				} else {
					?><option value="15j">Prévision à 15 jours</option><?
				}
			
		?></select>
		</p>
        <p>
		<label for="<?php echo $this->get_field_id( 'pagemeteo' ); ?>"><?php _e( 'URL page Météo:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'pagemeteo' ); ?>" name="<?php echo $this->get_field_name( 'pagemeteo' ); ?>" type="text" value="<?php echo esc_attr( $pagemeteo ); ?>" />
		</p>
		<?php 
		}
			
		// Updating widget replacing old instances with new
		public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['mode'] = ( ! empty( $new_instance['mode'] ) ) ? strip_tags( $new_instance['mode'] ) : '';
		$instance['pagemeteo'] = ( ! empty( $new_instance['pagemeteo'] ) ) ? strip_tags( $new_instance['pagemeteo'] ) : '';
		return $instance;
		}
		} // Class smtmeteo_widget ends here
		
		// Register and load the widget
		function smtmeteo_load_widget() {
			register_widget( 'smtmeteo_widget' );
		}
		add_action( 'widgets_init', 'smtmeteo_load_widget' );
 
 ?>