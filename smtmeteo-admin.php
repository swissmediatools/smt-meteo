<?

		//	Plugin SMT Meteo
		//	Page d'administration
		
			?><div class="wrap">
            	<h2>SMT Météo</h2>
            		<div class="smtmeteo_admin_col1">
                    	<p>Le plugin SMTmeteo vous permet d'afficher la météo locale sur votre site web, à l'intérieur de pages, d'articles et également dans le widget que nous avons préparé pour vous.</p><p>Les données météos sont fournies par <A HREF="http://openweathermap.org" target="_blank">OpenWeatherMap</A>, un projet open-source de météo combinant les données de plusieurs fournisseurs internationaux majeurs. Un service gratuit pour une consommation raisonnable.</p>
                        <h3>Mode d'emploi</h3>
                        <p>Le guide d'utilisation de ce plugin se trouve en ligne, directement sur notre site web, et en Français.</p>
                        <h3>Version Pro</h3>
                        <p>Vous pourrez bientôt acheter en ligne une version <b>PRO</b> du plugin meteo proposant de nombreuses options supplémentaires.</p>
                    </div>
                	<div class="smtmeteo_admin_col2">
                    	<h3>Réglages du Plugin</h3>
                        <form method="post" action="options.php">
                        <?		settings_fields( 'smtmeteo-settings-group' ); ?>
                        <table class="form-table">
                            <tr valign="top">                            
                            	<th scope="row">Clé API</th>
                            	<td><input type="text" name="owm_api_key" value="<?php echo get_option('owm_api_key'); ?>" size="32"><br><small>Pour obtenir gratuitement une clé : <A HREF="http://openweathermap.org/login" target="_blank">cliquez ici</A>.</small></td>
                            </tr>
                            <tr valign="top">                            
                            	<th scope="row">Durée Cache</th>
                            	<td><select size="1" name="owm_cachemin"><?
                                
									$actualcachemin = get_option('owm_cachemin');
									
									$cptcachemin=5;
									while ($cptcachemin<65)
										{
											if ($cptcachemin==$actualcachemin)
												{
													?><option value="<?	print($cptcachemin);	?>" selected><?	print($cptcachemin);	?> min.</option><?
												} else {
													?><option value="<?	print($cptcachemin);	?>"><?	print($cptcachemin);	?> minutes</option><?
												}
											$cptcachemin = $cptcachemin + 5;
										}
								
								?></select><br><small>Pour éviter de surcharger le serveur qui fournit les données, vous devez choisir une mise en cache des données pendant 5 à 60 minutes.</small></td>
                            </tr>
                            <tr valign="top">                            
                            	<th scope="row">Unités</th>
                            	<td><select size="1" name="owm_units"><?
                                
									$owmunits=get_option('owm_units');
					
									if ($owmunits=="metric")
										{
											?><option value="metric" selected>Métrique</option><?	
										} else {
											?><option value="metric">Métrique</option><?	
										}
										
									if ($owmunits=="imperial")
										{
											?><option value="imperial" selected>Impérial</option><?	
										} else {
											?><option value="imperial">Impérial</option><?	
										}
								
								?></select></td>
                            </tr>
                            <tr valign="top">                            
                            	<th scope="row">GPS</th>
                            	<td>lat <input type="text" name="owm_gpslat" value="<? echo get_option('owm_gpslat'); ?>" size="10"> lon <input type="text" name="owm_gpslon" value="<? echo get_option('owm_gpslon'); ?>" size="10"></td>
                            </tr>
                            <tr valign="top">                            
                            	<th scope="row">AutoUpdate</th>
                            	<td><?
                                	$smtupdate=get_option('smt_update');
									
									?><input type="checkbox" value="1" name="smt_update"<?	if ($smtupdate==1)	{	?> checked<?	}	?>><small>En cochant cette case, les nouvelles versions automatiquement installées.</small><?
									
								?></td>
                            </tr>
                        </table>
                        <?		submit_button(); 	?>
                        </form>
                        
                    </div>
                <div id="clear"></div>
        	</div><?



?>